<?php declare(strict_types=1);

namespace JohnSear\ComposerPackages;

class ComposerPackageManager
{
    /**
     * @var ComposerPackagesBuilder
     */
    private $composerPackagesBuilder;

    private $composerPackages;

    public function __construct(ComposerPackagesBuilder $composerPackagesBuilder)
    {
        $this->composerPackagesBuilder = $composerPackagesBuilder;
    }

    /**
     * @return ComposerPackage[]
     */
    public function getComposerPackages(): array
    {
        if ($this->composerPackages === null) {
            $this->composerPackages = $this->composerPackagesBuilder->buildComposerPackages();
        }

        return $this->composerPackages;
    }
}
