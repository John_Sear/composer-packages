<?php declare(strict_types=1);

namespace JohnSear\ComposerPackages;

interface ComposerPackageInterface
{
    public function getNameSpace(): string;
    public function setNameSpace(string $nameSpace): ComposerPackageInterface;

    public function getName(): string;
    public function setName(string $name): ComposerPackageInterface;

    public function getDisplayName(): string;
    public function setDisplayName(string $name): ComposerPackageInterface;

    public function getDescription(): string;
    public function setDescription(string $description): ComposerPackageInterface;

    public function getType(): string;
    public function setType($type): ComposerPackageInterface;

    public function getVersion(): string;
    public function setVersion(string $version): ComposerPackageInterface;
}
