<?php declare(strict_types=1);

namespace JohnSear\ComposerPackages;

class ComposerPackage implements ComposerPackageInterface
{
    private $nameSpace;
    private $name;
    private $displayName;
    private $description;
    private $type;
    private $version;

    public function getNameSpace(): string
    {
        return (string) $this->nameSpace;
    }

    public function setNameSpace(string $nameSpace): ComposerPackageInterface
    {
        $this->nameSpace = $nameSpace;

        return $this;
    }

    public function getName(): string
    {
        return (string) $this->name;
    }

    public function setName(string $name): ComposerPackageInterface
    {
        $this->name = $name;

        return $this;
    }

    public function getDisplayName(): string
    {
        return (string) $this->displayName;
    }

    public function setDisplayName(string $displayName): ComposerPackageInterface
    {
        $this->displayName = $displayName;

        return $this;
    }

    public function getDescription(): string
    {
        return (string) $this->description;
    }

    public function setDescription(string $description): ComposerPackageInterface
    {
        $this->description = $description;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType($type): ComposerPackageInterface
    {
        $this->type = $type;

        return $this;
    }

    public function getVersion(): string
    {
        return (string) $this->version;
    }

    public function setVersion(string $version): ComposerPackageInterface
    {
        $this->version = $version;

        return $this;
    }
}
