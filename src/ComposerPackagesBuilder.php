<?php declare(strict_types=1);

namespace JohnSear\ComposerPackages;

class ComposerPackagesBuilder
{
    /**
     * @return ComposerPackage[]
     */
    public function buildComposerPackages(): array
    {
        $composerPackages = [];

        $composerData = $this->getComposerInstalledPackagesRawData();
        if ($composerData === null || ! array_key_exists('packages', $composerData)) {
            return $composerPackages;
        }

        foreach ($composerData['packages'] as $package) {
            $displayName = (array_key_exists('name', $package))        ? $package['name']        : '';
            $description = (array_key_exists('description', $package)) ? $package['description'] : '';
            $type        = (array_key_exists('type', $package))        ? $package['type']        : '';
            $version     = (array_key_exists('version', $package))     ? $package['version']     : '';

            $displayNameParts = preg_split('#/#', $displayName, -1, PREG_SPLIT_NO_EMPTY);
            $nameSpace = $displayNameParts[0];
            $name = (array_key_exists(1,$displayNameParts)) ? $displayNameParts[1] : '';

            if ($name !== '' && $version !== '') {
                $composerPackages[] = (new ComposerPackage())
                    ->setNameSpace($nameSpace)
                    ->setName($name)
                    ->setDisplayName($displayName)
                    ->setDescription($description)
                    ->setType($type)
                    ->setVersion($version);
            }
        }

        return $composerPackages;
    }

    public function getComposerInstalledPackagesRawData(): array
    {
        $ds = DIRECTORY_SEPARATOR;
        $composerVendorPath = str_replace('johnsear/composer-packages/src', '', __DIR__);

        $composerInstalledPackagesFile = rtrim($composerVendorPath, $ds) . $ds . 'composer' . $ds . 'installed.json';

        $composerInstalledPackagesRawData = [];
        if (file_exists($composerInstalledPackagesFile)) {
            $composerInstalledPackagesFileContents = file_get_contents($composerInstalledPackagesFile);
            $composerInstalledPackagesRawData = json_decode($composerInstalledPackagesFileContents, true);
        }

        return $composerInstalledPackagesRawData;
    }
}
